fn main() {
    let s = String::from("Hello, world!");

    let i = first_word1(&s);

    let (a, b) = s.split_at(i);

    println!("a : {}, b : {}", a, b );
    
    // Although this works, it requires an extra variable which records a piece of 
    // state from s. The value however is not bound to the value of s
    // after the first_word1 returns. This can cause bugs as the two - the 's' and 
    // the state 'i' can fall out of sync

    // To aid this, rust introduces a type called Slice
    let t = String::from("hello there I am here");
    let hello = &t[0..5];
    let there = &t[6..11];

    println!("t : {}", t);
    println!("hello : {}, there: {}", hello, there);

    // Note: String slice range indices must occur at valid UTF-8 character boundaries. 
    // If you attempt to create a string slice in the middle of a multibyte character, 
    // your program will exit with an error. For the purposes of introducing string 
    // slices, we are assuming ASCII only in this section; a more thorough discussion 
    // of UTF-8 handling is in the “Storing UTF-8 Encoded Text with Strings” section 
    // of Chapter 8.

    let v = String::from("hello");
    let hello2 = &v[0..5];
    let start = &v[..3];
    let end = &v[3..];
    println!("hello2 : {}", hello2);
    println!("start : {}, end : {}", start, end);

    // First word with slices
    let w = String::from("howdy there");
    let fw = first_word(&w);
    println!("{}", fw);

    // A slice of string literal
    let z = "lalaaa daa";
    let z_slice = &z[4..];
    println!("A slice of string literal : {}", z_slice);
    
    // An improved first_word signature
    let x1 = String::from("Try this one");
    let x2 = "And for this one";
    let x1w = first_word_improved(&x1[..]);
    let x2w = first_word_improved(&x2);
    println!("x1w : {}", x1w);
    println!("x2w : {}", x2w);

    // A slice of an integer array
    let ia = [1, 2, 3, 4, 5];
    let ia1 = &ia[1..4];
    println!("{:?}", ia1);

    // But what is this.. size of ia is known, but when slicing, then size for ia2 is not known.... odd
    let ia2 = ia[1..4];  
    
}

fn first_word1(s: &String) -> usize {
    let bytes = s.as_bytes();

    for (i, &item) in bytes.iter().enumerate() {
        if item == b' ' {
            return i;
        }
    }
    s.len()
}

fn first_word(s: &String) -> &str {
    let bytes = s.as_bytes();
    for (i, &item) in bytes.iter().enumerate() {
        if item == b' ' {
            return &s[..i];
        }
    }

    &s[..]
}

fn first_word_improved(s: &str) -> &str {
    let bytes = s.as_bytes();
    for (i, &item) in bytes.iter().enumerate() {
        if item == b' ' {
            return &s[..i];
        }
    }

    &s[..]
}