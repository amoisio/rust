fn main() {
    println!("Two ways to calculate and print string length");

    println!("Way 1: giving and taking ownership");
    let s1 = String::from("Calculate this");
    let result1 = calculate_length1(s1);
    println!("str:'{}', len: {} ",result1.1, result1.0);
    
    println!("Way 2: Using references");
    let s2 = String::from("Then see if you can calculate this");
    let result2 = calculate_length2(&s2);
    println!("str:'{}', len: {} ",s2, result2);

    println!("Changing a referenced variable without mut won't work.");
    let s3 = String::from("try to change me!");
    // Changing wont work because the reference is immutable too 
    //change_reference(&s3);

    println!("Changing a referenced variable, with mut.");
    let mut s4 = String::from("Try to change me!");
    println!("s4 before : {}", s4);
    change_mut_reference(&mut s4);
    println!("s4 after : {}", s4);

    println!("Rust does not allow multiple simultaneous references to mutatable reference. (think ownership invalidation)");
    // This means we can have multiple simultaneous reference in _different_ scopes

    let mut s5 = String::from("I am a mutable string");

    // Note the below is completely fine because the string cannot be mutated
    // with the reference
    let r3 = &s5;
    let r4 = &s5;
    println!("r3 : {}, r4 : {}", r3, r4);

    // These will not compile
    let r1 = &mut s5;
    println!("r1 before r2 assignment : {}", r1);
    // let r2 = &mut s5;
    // println!("r2 after assignment : {}", r2);
    // println!("r1 after r2 assingnment : {}", r1);
    
    
}


fn calculate_length1(input: String) -> (usize, String) {
    let len : usize = input.len();

    (len, input)
}

fn calculate_length2(input: &String)-> usize {
    input.len()
}

// This won't even compile because input is not mutable
// fn change_reference(input: &String) {
//     input.push_str(", done!");
// }

fn change_mut_reference(input: &mut String) {
    input.push_str(" I think I did");
}

fn create_dangling_pointer() -> &String {
    let s = String::from("test");

    &s
}